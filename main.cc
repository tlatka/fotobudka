#include "src/Manager.hh"

#include <stdio.h>

int main(int argc, char** argv)
{
    printf("Hello World\n");

    fb::Manager mainManager;
    mainManager.Start();

    return 0;
}
