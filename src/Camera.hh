/*
 * Fotobudka
 * Tomasz Latka
 * tomasz_latka@yahoo.com
 */

#ifndef FB_CAMERA
#define FB_CAMERA

#include "opencv2/opencv.hpp"

namespace fb {


class Camera
{
public:
    Camera();
    ~Camera();
    cv::Mat GetFrame();
private:
    cv::VideoCapture* m_Cap;
};

}   // namspace fb

#endif
