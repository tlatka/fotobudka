#include "Camera.hh"
#include <stdio.h>

namespace fb
{
Camera::Camera()
{
    printf("Hello World camera\n");
    m_Cap = new cv::VideoCapture();
    if( !m_Cap->open(0) ) {
        printf("ERROR: Can't open video capture\n");
    }
}
Camera::~Camera()
{
    delete m_Cap;
    printf("Bye Bye camera\n");
}
cv::Mat Camera::GetFrame()
{
    cv::Mat frame;
    m_Cap->operator >>(frame);
    return frame;
}
}   // namespace fb
