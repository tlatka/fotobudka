#include "Manager.hh"
#include <stdio.h>

namespace fb
{
Manager::Manager()
{
    printf("Hello World Manager\n");
    m_Camera = new Camera();
    m_Display = new Display();
}
Manager::~Manager()
{
    delete m_Camera;
    delete m_Display;
    printf("Bye Bye Manager\n");
}

void Manager::Start()
{
    for(;;) {
        cv::Mat frame;
        frame = m_Camera->GetFrame();
        m_Display->Update(frame);

        if( cv::waitKey(1) == 27 ) break;
    }
}

}   // namespace fb
