/*
 * Fotobudka
 * Tomasz Latka
 * tomasz_latka@yahoo.com
 */

#ifndef FB_DISPLAY
#define FB_DISPLAY

#include "opencv2/opencv.hpp"
#include <string>

namespace fb {

class Display
{
public:
    Display();
    ~Display();

    void Update(cv::Mat& a_Mat);

private:
    const std::string m_MainWindow;
};

}   // namspace fb

#endif
