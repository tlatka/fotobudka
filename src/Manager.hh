/*
 * Fotobudka
 * Tomasz Latka
 * tomasz_latka@yahoo.com
 */

#ifndef FB_MANAGER
#define FB_MANAGER

#include "Camera.hh"
#include "Display.hh"

namespace fb {

class Manager
{
public:
    Manager();
    ~Manager();

    void Start();

private:
    Camera* m_Camera;
    Display* m_Display;
};

}   // namspace fb

#endif
