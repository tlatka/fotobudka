#include "Display.hh"

namespace fb
{

Display::Display() : m_MainWindow("MainWindow")
{
}

Display::~Display()
{
}

void Display::Update(cv::Mat& a_Frame)
{
    if( a_Frame.empty() ) return; // end of video stream
        cv::imshow(m_MainWindow, a_Frame);
}

}
